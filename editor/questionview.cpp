#include "questionview.h"
#include "checklabelbox.h"
#include "editablemodel.h"

#include <QAbstractItemModel>
#include <QCheckBox>
#include <QDebug>
#include <QFile>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QRadioButton>
#include <QSplitter>
#include <QStandardItemModel>
#include <QString>
#include <QTextEdit>
#include <QVariant>

#include <KLocale>
#include <KPushButton>

QuestionView::QuestionView ( QWidget* parent )
	: QWidget ( parent )
{
 	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	splitMain = new QSplitter;
	splitMain->setOrientation(Qt::Vertical);
	splitMain->setChildrenCollapsible(false);
	
	statement = new QTextEdit (i18n("Open a questions file"));
	statement->setMinimumHeight(100);
	splitMain->addWidget(statement);
	
	widgetDownSplit = new QWidget;
	QVBoxLayout* mainSplitterLayout = new QVBoxLayout;
	widgetDownSplit->setLayout(mainSplitterLayout);
	splitMain->addWidget(widgetDownSplit);
	
	radioGroup = new QGroupBox;
	mainSplitterLayout->addWidget(radioGroup);
	
	KPushButton* buttonAddAnswer = new KPushButton("Add answer");
	buttonAddAnswer->setIcon(KIcon("list-add"));
	KPushButton* buttonDelAnswer = new KPushButton("Remove answer");
	buttonDelAnswer->setIcon(KIcon("list-remove"));

	connect (buttonAddAnswer, SIGNAL(clicked()), this, SLOT(addAnswer()) );
	connect (buttonDelAnswer, SIGNAL(clicked()), this, SLOT(delAnswer()) );
	
	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	mainSplitterLayout->addLayout(buttonsLayout);
	buttonsLayout->addWidget(buttonAddAnswer);
	buttonsLayout->addWidget(buttonDelAnswer);

	radioL = new QVBoxLayout;
	
	listCheckBoxes = new QList<CheckLabelBox* >;

	for(int i=0 ; i < 4 ; ++i ) 
	{
		QString letter('A'+i);
		CheckLabelBox* clb = new CheckLabelBox(letter);
		listCheckBoxes->append(clb);
		radioL->addWidget(clb);
	}
	radioGroup->setLayout(radioL);
	mainLayout->addWidget(splitMain);
	
	loaded = false;
}

QuestionView::~QuestionView() {}

void QuestionView::loadQuestion (const QModelIndex& item, bool saveData)
{
	actualItem = item;
	if (loaded)
	{
		if (!listCheckBoxes->isEmpty() && oldItem->isValid())
		{
			if (saveData)
				saveActualData();
			delete oldItem;
		}
	}
	loaded = true;

	qDeleteAll(listCheckBoxes->begin(),listCheckBoxes->end());
	listCheckBoxes->clear();
	
	for(int i=0 ; i < model->rowCount(item) ; ++i ) 
	{
		QString letter('A'+i);
		
		CheckLabelBox* clb = new CheckLabelBox(letter);
		listCheckBoxes->append(clb);
		clb->setText(model->data(item.child(i,0)).toString());
		QVariant isOk = model->data (item.child ( i,0 ), Qt::UserRole );
		if (isOk == "True")
			clb->setOk();
		radioL->addWidget(clb);
	}
	if ( QString (model->data((item), Qt::UserRole).toString()).isEmpty() )
		statement->setText (model->data(item).toString());
	else
	{
		QFile f (model->data((item), Qt::UserRole).toString());
		if ( f.exists() ) 
		{
			statement->setText("");
			QString htmlImg(model->data(item).toString()+"<br><br><img src="+model->data((item), Qt::UserRole).toString()+">");
			statement->insertHtml(htmlImg);
		}
		else 
		statement->setText(model->data(item).toString());
	}
	oldItem = new QModelIndex(item);
}

void QuestionView::setModel ( EditableModel* im ) { model = im; }

void QuestionView::saveActualData ()
{
	model->setQuestionData(*oldItem, statement->toPlainText());
	
	QList<CheckLabelBox*>::const_iterator cb;
	int i=0;
	for (cb=listCheckBoxes->begin(); cb != listCheckBoxes->end(); ++cb, ++i )
	{
		model->setQuestionData(oldItem->child(i, 0), (*cb)->text());
		model->setQuestionData(oldItem->child(i, 0), (*cb)->checkState(), Qt::UserRole);
	}
}

void QuestionView::storeLastItem()
{
	if (loaded) saveActualData();
}

void QuestionView::clearGUI()
{
	statement->setText ( i18n("Add new Questions File..."));
	
	QList<CheckLabelBox*>::const_iterator cb;
	for (cb=listCheckBoxes->begin(); cb != listCheckBoxes->end(); ++cb)
	{
		(*cb)->setText(i18n("Empty..."));
	}

	loaded = false;
}

void QuestionView::addAnswer()
{
	model->appendAnswer(actualItem);
	loadQuestion(actualItem);
}

void QuestionView::delAnswer()
{
	model->deleteAnswer(actualItem);
	loadQuestion(actualItem);
}


void QuestionView::clearQuestion()
{
	statement->setText("");
	
	QList<CheckLabelBox*>::const_iterator cb;
	for (cb=listCheckBoxes->begin(); cb != listCheckBoxes->end(); ++cb)
	{
		(*cb)->setText("");
	}
	
}
