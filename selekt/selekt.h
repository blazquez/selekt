#ifndef SELEKT_H
#define SELEKT_H

#include "questionview.h"

#include <QListView>
#include <KXmlGuiWindow>

class QAbstractItemModel;
class QListView;
class QModelIndex;
class QString;
class QTime;
class KAction;

class Selekt : public KXmlGuiWindow
{
	Q_OBJECT
	public:
		Selekt (QWidget* parent=0);
		virtual ~Selekt();

	private slots:
		void slotLoadFile();
		void slotUpdateGUI(const QModelIndex& item);
		void slotResult();
		void slotNextQuestion();
		void slotPreviousQuestion();
	private:
		void setMenu();
		
		QString path, oldPath;
		QListView* m_listView;
		QuestionView* m_questionView;
		QAbstractItemModel* model;
		QModelIndex actualItem;
		QMenu* menuSettings;
		bool isLoaded;
		QTime* timeElapsed;
		
		KAction* nextQuestion;
		KAction* previousQuestion;
		KAction* result; 
};

#endif
