/*
    Copyright (C) 2009 Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CHECKLABELBOX_H
#define CHECKLABELBOX_H

#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>

#include <KPushButton>

class CheckLabelBox : public QWidget
{
	Q_OBJECT
	public:
		CheckLabelBox ( QString letter, QWidget* parent = 0);
		~CheckLabelBox ();
		void setText(QString text);
		QString text ();
		QString checkState();
	public slots:
		void setOk();
		void setFalse();
	private slots:
	private:
		QHBoxLayout* mainLayout;
		QLabel* cb;
		QTextEdit* te;
		KPushButton* addOk;
		KPushButton* addFalse;
};

#endif // CHECKLABELBOX_H
