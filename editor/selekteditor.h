/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SELEKTEDITOR_H
#define SELEKTEDITOR_H

#include "questionview.h"
#include "listeditor.h"
#include "editablemodel.h"

#include <KXmlGuiWindow>

class QString;

class SelektEditor : public KXmlGuiWindow
{
	Q_OBJECT
	public:
		SelektEditor (QWidget* parent=0);
		virtual ~SelektEditor();

	private slots:
		void newFile();
		void loadFile();
		void saveFile();
		void saveFileAs();
		void updateGUI (const QModelIndex& item, bool saveData = true);
		void clearGUI();
		void slotAddQuestion();
		void slotAddAnswer();
		void slotRemoveQuestion();
		void slotRemoveAnswer();
		void slotNextQuestion();
		void slotPreviousQuestion();
	private:
		void setMenu();
		
		QString path;
		ListEditor* m_listView;
		QuestionView* m_questionView;
		EditableModel* model;
		QMenu* menuSettings;
		bool isLoaded;
		QModelIndex actualIdx;
};

#endif
