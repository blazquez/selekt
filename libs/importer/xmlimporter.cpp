/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "importerfactory.h"
#include "xmlimporter.h"

#include <QDebug>
#include <QDomDocument>
#include <QFile>
#include <QFileInfo>
#include <QStandardItemModel>

REGISTER_IMPORTER("kst", XmlImporter)

QAbstractItemModel* XmlImporter::import(const QString& _path, QObject* parent)
{
	QStandardItemModel* m=new QStandardItemModel(parent);
	path = _path;
	QFile file(path);
	if (!file.open(QIODevice::ReadOnly))
	{
		qDebug()<< "Can't read the file" << path;
		return m;
	}

	QDomDocument doc("tests");
	if (!doc.setContent(&file))
	{
		file.close();
		qDebug()<< "Can't read XML file";
		return m;
	}
	file.close();

	QDomElement docElem = doc.documentElement();
	QDomNode n = docElem.firstChild();

	for(int i=0 ; !n.isNull() ; n = n.nextSibling(), ++i )
	{
		QDomElement e = n.toElement();
		if(!e.isNull() && e.tagName()=="question")
		{
			QStandardItem* aItem = readItem (e);
			if (aItem != NULL)  m->appendRow(aItem);
		}
	}

	file.close();

	return m;
}

QStandardItem *XmlImporter::readItem (const QDomElement& docElem)
{
	bool ok = false;
	QStandardItem* aItem = new QStandardItem;
	QDomNode n = docElem.firstChild();
	int i=0;
	if (n.isElement()){
		for ( ; !n.isNull() ; n = n.nextSibling() )
		{
			QDomElement e = n.toElement();
			if (e.tagName() == "statement")
			{
				aItem->setText(e.text());
				ok = true;
			}
			if (e.tagName() == "image")
			{
				if (e.text().startsWith("http://") || e.text().startsWith("www.") )
					aItem->setData(e.text(), Qt::UserRole);
				else
				{
					QString imgPath (QFileInfo (path).canonicalPath() + "/" + e.text ());
					aItem->setData (imgPath, Qt::UserRole);
				}
			}
			if (e.tagName() == "answer")
			{
				QString tmp = e.text();
				QStandardItem* tmpItem = new QStandardItem(tmp);
				if (e.hasAttribute("correct") && e.attribute("correct") == "1")
					tmpItem->setData("True",Qt::UserRole);
				aItem->appendRow(tmpItem);
				++i;
			}
		}
	}
	if (ok && i>1)
		return aItem;
	else
	{
		delete aItem;
		return NULL;
	}
}

