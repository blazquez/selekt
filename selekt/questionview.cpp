#include "questionview.h"
#include "checklabelbox.h"

#include <QAbstractItemModel>
#include <QCheckBox>
#include <QDebug>
#include <QFile>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QSplitter>
#include <QString>
#include <QVariant>

#include <KHTMLPart>
#include <KLocale>

QuestionView::QuestionView ( QWidget* parent )
	: QWidget ( parent )
{
 	QVBoxLayout* mainLayout = new QVBoxLayout(this);
	
	statement = new KHTMLPart (this); 
	statement->begin();
	statement->write (i18n ("Open a questions file"));
	statement->end();

	radioL = new QVBoxLayout;
	radioGroup = new QGroupBox;
	radioGroup->setLayout(radioL);

	lcb = new QList<CheckLabelBox* >;

	for(int i=0 ; i < 4 ; ++i ) 
	{
		QString letter('A'+i);
		CheckLabelBox* clb = new CheckLabelBox(letter);
		lcb->append(clb);
		radioL->addWidget(clb);
	}
	splitMain = new QSplitter;
	splitMain->setOrientation(Qt::Vertical);
	splitMain->setChildrenCollapsible(false);
	QWidget* preg = statement->widget();
	preg->setMinimumHeight(150);
	splitMain->addWidget(preg);
	splitMain->addWidget(radioGroup);
	mainLayout->addWidget(splitMain);
	
	loaded = false;
}

void QuestionView::loadQuestion (const QModelIndex& item )
{
	if (loaded)
	{
		if (!lcb->isEmpty() && oldItem->isValid())
		{
			checkState();
			delete oldItem;
		}
	}
	loaded = true;

	qDeleteAll(lcb->begin(),lcb->end());
	lcb->clear();
	
	splitMain->addWidget(radioGroup);
	for(int i=0 ; i < model->rowCount(item) ; ++i ) 
	{
		QString letter('A'+i);
		CheckLabelBox* clb = new CheckLabelBox(letter);
		lcb->append(clb);
		clb->setText(model->data(item.child(i,0)).toString());
		QVariant state = model->data(item.child(i,0),Qt::CheckStateRole);
		if (state == "check")
		{
			clb->setChecked(true);
		}
		radioL->addWidget(clb);
		QVariant ok = model->data(item.child(i,0),Qt::UserRole);
	}
	statement->begin();
	if ( QString(model->data((item),Qt::UserRole).toString()).isEmpty() )
		statement->write(model->data(item).toString());
	else
	{
		QFile f (model->data((item),Qt::UserRole).toString());
		statement->write("");
		QString htmlImg(model->data(item).toString()+"<br><br><img src="+model->data((item),Qt::UserRole).toString()+">");
		statement->write(htmlImg);
	}
	statement->end();
	oldItem = new QModelIndex(item);
}

void QuestionView::setModel ( QAbstractItemModel* im ) { model = im; }

void QuestionView::checkState ()
{
	QList<CheckLabelBox*>::const_iterator cb;
	int i=0;
	for (cb=lcb->begin(); cb != lcb->end(); ++cb, ++i )
	{
		QVariant state;
		if ( (*cb)->checkState())
			state = "check";
		else 
			state.setValue<QString>("notCheck");
		if (!(model->setData (oldItem->child (i,0), state, Qt::CheckStateRole )))
			qDebug() << "Data not saved";
	}
}

void QuestionView::storeLastItem()
{
	if (loaded) checkState();
}


void QuestionView::clearModel()
{
	delete model;
	clearGUI();
}

void QuestionView::clearGUI()
{
	statement->begin();
	statement->write(i18n("Add new Questions File..."));
	statement->end();
	
	QList<CheckLabelBox*>::const_iterator cb;
	for (cb=lcb->begin(); cb != lcb->end(); ++cb)
	{
		(*cb)->setText (i18n ("Empty answer..."));
		(*cb)->setChecked (false);
	}

	loaded = false;
}
