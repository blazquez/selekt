/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ABSTRACTIMPORTER_H
#define ABSTRACTIMPORTER_H

#include <kdemacros.h>

class QAbstractItemModel;
class QStandardItemModel;
class QString;
class QObject;

class KDE_EXPORT AbstractImporter
{
	public:
		virtual ~AbstractImporter() {}
		virtual QAbstractItemModel* import(const QString& path, QObject* parent=0) =0;
};

#endif // ABSTRACTIMPORTER_H
