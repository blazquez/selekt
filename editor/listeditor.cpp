#include "listeditor.h"

#include <KLocale>
#include <KPushButton>

#include <QDebug>
#include <QStandardItem>
#include <QVBoxLayout>

ListEditor::ListEditor ( QWidget* parent )
	: QWidget( parent )
{
	QVBoxLayout* mainLayout = new QVBoxLayout (this);
	m_listView = new QListView(this);
	m_listView->setAlternatingRowColors(true);
	m_listView->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
	m_listView->setTextElideMode (Qt::ElideRight); 
	m_listView->setEditTriggers (QAbstractItemView::NoEditTriggers);

	KPushButton* buttonAddQuestion = new KPushButton (i18n ("&Add"), this);
	buttonAddQuestion->setIcon (KIcon ("archive-insert"));
	KPushButton* buttonDelQuestion = new KPushButton (i18n ("&Remove"), this);
	buttonDelQuestion->setIcon (KIcon ("archive-remove"));
	
	QHBoxLayout* buttonsLayout = new QHBoxLayout;
	buttonsLayout->addWidget (buttonAddQuestion);
	buttonsLayout->addWidget (buttonDelQuestion);
	
	mainLayout->addWidget (m_listView);
	mainLayout->addLayout (buttonsLayout);
	this->setLayout(mainLayout);
	
	connect (buttonAddQuestion, SIGNAL(clicked()), this, SLOT(addQuestion()) );
	connect (buttonDelQuestion, SIGNAL(clicked()), this, SLOT(delQuestion()) );
	
	connect ( m_listView, SIGNAL ( clicked ( QModelIndex ) ), this, SLOT ( emitNormalSignal ( QModelIndex ) ) );
	connect ( m_listView, SIGNAL ( activated (QModelIndex) ), this, SLOT ( emitNormalSignal ( QModelIndex ) ) );
}

ListEditor::~ListEditor() {}

void ListEditor::setModel ( EditableModel* model )
{
	m_model = model;
	m_listView->setModel (model);
	m_listView->setCurrentIndex (m_model->index(0,0));
}

void ListEditor::emitSignal ( const QModelIndex& item, const bool saveData )
{
	actualItem = item;
	emit activated (item, saveData);
}

void ListEditor::emitNormalSignal (const QModelIndex& idx)
{
	actualItem = idx;
	emit activated (idx, true);
}

void ListEditor::addQuestion ()
{
	m_model->appendQuestion();
}

void ListEditor::delQuestion ()
{
// 	if (m_model->rowCount () > 1)
// 		m_model->removeRow (actualItem);
	m_model->deleteQuestion(actualItem);
	
	m_listView->setCurrentIndex (m_model->index(0,0));
	emitSignal (m_model->index (0,0), false);
}

void ListEditor::setCurrentIndex(const QModelIndex& index)
{
	m_listView->setCurrentIndex(m_model->index(index.row(),0));
}
