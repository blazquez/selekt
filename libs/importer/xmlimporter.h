/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef XMLIMPORTER_H
#define XMLIMPORTER_H

#include "abstractimporter.h"

#include <kdemacros.h>
#include <QString>

class QStandardItem;
class QDomElement;



class KDE_EXPORT XmlImporter : public AbstractImporter
{
	public:
		virtual QAbstractItemModel* import(const QString& _path, QObject* parent = 0);
	private:
		QStandardItem *readItem (const QDomElement& docElem);
		QString path;
};

#endif // XMLIMPORTER_H
