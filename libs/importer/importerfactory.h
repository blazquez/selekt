/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef IMPORTERFACTORY_H
#define IMPORTERFACTORY_H

#include <kdemacros.h>

#include <QMap>
#include <QString>
#include <QStringList>

class AbstractImporter;
class QObject;

#define REGISTER_IMPORTER(ext, classname) \
	AbstractImporter* create##classname () { return new classname; } \
	bool b##classname = ImporterFactory::self()->registerImporter(ext, create##classname);

class KDE_EXPORT ImporterFactory
{
	public:
		typedef AbstractImporter* (*ImporterCreator)();
		typedef QString Key;
		static ImporterFactory* self();
		QStringList extensions() const;
		
		bool registerImporter(const Key& key, ImporterCreator);
		AbstractImporter* get(const Key& key) const;
	private:
		ImporterFactory();
		static ImporterFactory* m_self;
		
		QMap<Key, ImporterCreator> m_importers;
};

#endif // IMPORTERFACTORY_H
